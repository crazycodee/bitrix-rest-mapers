<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\IO\FileNotFoundException;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Crazy\Code\Handlers\Rest\Crm;

Loc::loadMessages(__FILE__);

class CrazyCode extends CModule #NOSONAR
{
    public function DoInstall(): void
    {
        $this->InstallEvents();
    }

    /**
     * @return void
     * @throws LoaderException
     * @throws ReflectionException
     * @throws FileNotFoundException
     */
    public function InstallEvents(): void
    {
        $event = EventManager::getInstance();

        $event->registerEventHandler(
            'rest',
            'OnRestServiceBuildDescription',
            $this->MODULE_ID,
            Crm::class,
            'onRestServiceBuildDescription'
        );
    }
}
