<?php
/** @noinspection SpellCheckingInspection */

namespace Crazy\Code\Handlers\Rest\Maps;

use Bitrix\Landing\Domain;

class ProductMapFields extends BaseMapFields
{
    public static function setSelectedFields(): array
    {
        // названия свойств и их ключей придумывал не я
        return [
            'NAME'                                => 'NAME',
            'isrppFieldslDs'                      => [],
            'ID'                                  => 'ID',
            'SORT'                                => 'SORT',
            'LinkNaFormuProducta'                 => 'LINK_NA_FORMU_PRODUCTA.VALUE',
            'ApplicationTemplate'                 => [],
            'TypeProduct'                         => 'TYPE_PRODUCT.ITEM.VALUE',
            'Podcategory'                         => [],
            'OtobrazhatVLkMobilePodcategoriya'    => 'OTOBRAZHAT_V_LK_MOBILE_PODCATEGORIYA.ITEM.VALUE',
            'KratkoeNazvanie'                     => 'KRATKOE_NAZVANIE.VALUE',
            'UrlProductPicture'                   => 'URL_PRODUCT_PICTURE.VALUE',
            'KratkoyeOpisaniyeUslugiMassiv'       => [
                'KratkoyeOpisaniyeUslugi'          => 'KRATKOYE_OPISANIYE_USLUGI.VALUE',
                'KratkoyeOpisaniyeUslugiZagolovok' => 'KRATKOE_OPISANIE_USLUGI.VALUE',
            ],
            'KtoMozhetPoluchitMassiv'             => [
                'KtoMozhetPoluchit'             => 'KTO_MOZHET_POLUCHUT.VALUE',
                'KtoMozhetPoluchitZagolovok'    => 'KTO_MOZHET_POLUCHIT_ZAGOLOVOK.VALUE',
                'KtoMozhetPoluchitPodZagolovok' => 'KTO_MOZHET_POLUCHIT_PODZAGOLOVOK.VALUE',
            ],
            'KakiyeZatratyKompensiruyutsyaMassiv' => [
                'KakiyeZatratyKompensiruyutsya'             => 'KAKIYE_ZATRATY_KOMPENSIRUYUTSYA.VALUE',
                'KakiyeZatratyKompensiruyutsyaZagolovok'    => 'KAKIE_ZATRATI_KOMPENSIRUYTSA_ZAGOLOVOK.VALUE',
                'KakiyeZatratyKompensiruyutsyaPodzagolovok' => 'KAKIE_ZATRATI_KOMPENSIRUYTSA_PODZAGOLOVOK.VALUE',
                'KakiyeZatratyKompensiruyutsyaFileMassiv'   => [
                    'KakiyeZatratyKompensiruyutsyaFileName' => 'KAKIE_ZATRATI_KOMPENSIRUYTSA_FILE.FILE.FILE_NAME',
                    'KakiyeZatratyKompensiruyutsyaFileLink' => 'KAKIE_ZATRATI_KOMPENSIRUYTSA_FILE.FILE.SUBDIR',
                ],
            ],
            'RezultatMassiv'                      => [
                'Rezultats'            => 'REZULTAT.VALUE',
                'RezultatZagolovok'    => 'REZULTAT_ZAGOLOVOK.VALUE',
                'RezultatPodzagolovok' => 'REZULTAT_PODZAGOLOVOK.VALUE',
            ],
            'RazmerKompensatsiiMassiv'            => [
                'RazmerKompensatsii'             => 'RAZMER_KOMPENSATSII.VALUE',
                'RazmerKompensatsiiZagolovok'    => 'RAZMER_KOMPENSACII_ZAGOLOVOK.VALUE',
                'RazmerKompensatsiiPodzagolovok' => 'RAZMER_KOMPENSACII_PODZAGOLOVOK.VALUE',
                'RazmerKompensatsiiFileMassiv'   => [
                    'RazmerKompensatsiiFileName' => 'RAZMER_KOMPENSACII_FILE.FILE.FILE_NAME',
                    'RazmerKompensatsiiFileLink' => 'RAZMER_KOMPENSACII_FILE.FILE.SUBDIR',
                ],
            ],
            'PeriodKompensatsiiMassiv'            => [
                'PeriodKompensatsii'           => 'PERIOD_KOMPENSATSII.VALUE',
                'PeriodKompensatsiiZagolovok'  => 'PERIOD_KOMPENSACII_ZAGOLOVOK.VALUE',
                'PeriodKompensatsiiFileMassiv' => [
                    'PeriodKompensatsiiFileName' => 'PERIOD_KOMPENSACII_FILE.FILE.FILE_NAME',
                    'PeriodKompensatsiiFileLink' => 'PERIOD_KOMPENSACII_FILE.FILE.SUBDIR',
                ],
            ],
            'OtobrazhatVLkWeb'                    => 'OTOBRAZHAT_V_LK_WEB.ITEM.VALUE',
            'OtobrazhatVLkMobile'                 => 'OTOBRAZHAT_V_LK_MOBILE.ITEM.VALUE',
            'IDUslugiIsRpp'                       => 'IS_RPP_ID',
            'IDChernovikaIsRpp'                   => 'ID_CHERNOVIKA_IS_RPP.VALUE',

            'Documenty'            => [],
            'DocumentyDlyaPodachi' => [],
            'StatusGroupProduct'   => [],
            'GroupStatusNotFound'  => [],

        ];
    }

    public static function prepareFields(
        $productItems,
        $fieldsSubcategories,
        $fieldsMappingIsRpp,
        $fieldsMapping
    ): array {
        $productMapItems = self::prepareFieldsRecursive(self::setSelectedFields(), $productItems);
        $productMapItems['Documenty'] = self::getFiles(['DocName', 'DocLink'], 'DOKUMENTY', $productItems);
        $productMapItems['DocumentyDlyaPodachi'] = self::getFiles(
            ['DocDlyaPodachiName', 'DocDlyaPodachiLink'],
            'DOCUMENTY_DLYA_PODACHI',
            $productItems
        );
        $productMapItems['ApplicationTemplate'] = self::getFile('APPLICATION_TEMPLATE', $productItems);
        self::getIDUslugiIsRpp($productMapItems);
        self::setPodcategory($productMapItems, $fieldsSubcategories);
        self::setStatusGroup($productMapItems, $fieldsMappingIsRpp);
        self::setFileLink($productMapItems);
        self::setMappingFields($productMapItems, $fieldsMapping);

        return $productMapItems;
    }

    private static function getIDUslugiIsRpp(&$productMapItems)
    {
        $isRppIds = [];
        foreach ($productMapItems['IDUslugiIsRpp']->getAll() as $items) {
            $isRppIds[] = $items->getValue();
        }
        if (!empty($isRppIds)) {
            $productMapItems['IDUslugiIsRpp'] = $isRppIds;
        } else {
            $productMapItems['IDUslugiIsRpp'] = null;
        }
    }

    private static function getFileLink(object $file, string $fileName): string
    {
        return Domain::getHostUrl()
            . '/upload/'
            . $file->getSubdir()
            . '/'
            . $fileName;
    }

    private static function getFiles(array $names, string $field, object $items): ?array
    {
        $documentyItem = [];
        foreach ($items->get($field)->getAll() as $item) {
            $fileName = $item->getFile()->getFileName();
            $documentyItem[] = [
                $names[0] => $fileName,
                $names[1] => self::getFileLink($item->getFile(), $fileName)
            ];
        }

        return $documentyItem;
    }



    private static function getFile(string $field, object $items): ?string
    {
        $fileLinks = '';
        $res = $items->get($field);
        if (isset($res)) {
            $file = $items->get($field)->getFile();
            if (isset($file)) {
                $fileName = str_replace(' ', '%20', $file->getFileName());
                $fileLinks = self::getFileLink($file, $fileName);
            }
        } else {
            $fileLinks = null;
        }
        return $fileLinks;
    }

    private static function setPodcategory(&$productItems, $fieldsSubcategories)
    {
        foreach ($fieldsSubcategories as $subcategory) {
            if ($subcategory->getProduct()->getElement()?->getId() === $productItems['ID']) {
                $productItems['Podcategory'][] = [
                    'NamePodcategory' => $subcategory->getName(),
                    'PodcategoryId'   => $subcategory->getId(),
                ];
            }
        }
    }

    private static function setStatusGroup(&$productItems, $fieldsMappingIsRpp)
    {
        foreach ($fieldsMappingIsRpp as $i) {
            if ($i->getMapProduct()->getElement()?->getId() === $productItems['ID']) {
                $statusGroupProduct = $i->get('MAP_STATUS_GROUP')?->getItem()->getValue();
                $result = [
                    'StatusName'         => $i->get('MAP_STATUS_CRM')?->getValue(),
                    'Active'             => $i->get('MAP_PUB_IN_LK')?->getItem()->getValue(),
                    'SerialNumber'       => $i->get('MAP_SERIAL_NUMBER')?->getValue(),
                    'MapCodeStatusIsRpp' => $i->get('MAP_CODE_STATUS_IS_RPP')?->getElement()?->getName(),
                    'MapNameStatusIsRpp' => $i->get('MAP_STATUS_IS_RPP')?->getValue(),
                ];
                if ($statusGroupProduct) {
                    $productItems['StatusGroupProduct']
                    [$statusGroupProduct][] = $result;
                } else {
                    $productItems['GroupStatusNotFound'][] = $result;
                }
            }
        }
    }

    private static function setFileLink(&$productItems): void
    {
        $links = [
            'KakiyeZatratyKompensiruyutsya',
            'RazmerKompensatsii',
            'PeriodKompensatsii',
        ];
        foreach ($links as $link) {
            $filePath = $productItems[$link . 'Massiv'][$link . 'FileMassiv'];
            $productItems[$link . 'Massiv'][$link . 'FileMassiv'][$link . 'FileLink']
                = $filePath[$link . 'FileLink'] ? Domain::getHostUrl()
                . '/upload/'
                . $filePath[$link . 'FileLink']
                . '/'
                . $filePath[$link . 'FileName']
                : '';
        }
    }

    private static function setMappingFields(&$productItems, $fieldsMapping)
    {
        foreach ($fieldsMapping as $field) {
            if ((int)$field['PRODUCT_ID'] === (int)$productItems['ID']) {
                $productItems['isrppFieldslDs'][] = [
                    'IdIsRpp' => $field['ID_FIELD_IS_RPP'],
                    'FieldName' => $field['FIELD_NAME'],
                    'Table'   => $field['TABLE_NAME'],
                ];
            }
        }
    }
}
