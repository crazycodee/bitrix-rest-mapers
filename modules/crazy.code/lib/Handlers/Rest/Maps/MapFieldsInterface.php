<?php

namespace Crazy\Code\Handlers\Rest\Maps;

interface MapFieldsInterface
{
    public static function setSelectedFields(): array;

    public static function getSelectedFields(): array;
}
