<?php

namespace Crazy\Code\Handlers\Rest\Maps;

use Exception;

class BaseMapFields implements MapFieldsInterface
{
    public static function setSelectedFields(): array
    {
        return [];
    }

    protected static function prepareFieldsRecursive($selected, $items): array|string
    {
        $result = [];

        foreach ($selected as $key => $selectedItem) {
            if (is_array($selectedItem)) {
                $result[$key] = self::prepareFieldsRecursive($selectedItem, $items);
            } else {
                try {
                    if (is_object($items)) {
                        foreach (explode('.', $selectedItem) as $structure) {
                            $result[$key] = empty($result[$key])
                                ? $items->get($structure)
                                : $result[$key]->get($structure);
                        }
                    } else {
                        $result[$key] = $items[$key];
                    }
                } catch (Exception $e) {
                    ShowError($e->getMessage());
                }

                if (!is_object($result[$key]) && str_starts_with($result[$key], 'a:2:{')) {
                    /** @noinspection UnserializeExploitsInspection */
                    $result[$key] = unserialize($result[$key])['TEXT'];
                }
            }
        }

        return $result;
    }

    public static function getSelectedFields(): array
    {
        $productFields = static::setSelectedFields();
        $result = [];

        if (!empty($productFields)) {
            array_walk_recursive($productFields, static function ($item, $key) use (&$result) {
                $result[$key] = $item;
            });
        }

        return $result;
    }
}
