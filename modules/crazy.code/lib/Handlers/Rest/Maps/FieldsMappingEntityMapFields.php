<?php
/**
 * @noinspection SpellCheckingInspection
 * @bxnolanginspection
 */

namespace Crazy\Code\Handlers\Rest\Maps;

class FieldsMappingEntityMapFields extends BaseMapFields
{
    public static function setSelectedFields(): array
    {
        // названия свойств и их ключей придумывал не я
        return [
            'ID' => 'ID',
            'MapPolProduct' => 'PRODUCT.NAME',
            'IdProduct' => 'PRODUCT.ID',
            'Podcategory' => [
                [
                    'NamePodcategory' => 'SUBCATEGORY.NAME',
                    'PodcategoryId' => 'SUBCATEGORY.ID',
                ],
            ],
            'FieldsInCrmMassiv' => [
                'MapPolNamePolCrm' => 'MAP_POL_NAME_POL_CRM.VALUE',
                'MapPolTypeCrm' => 'MAP_POL_TYPE_CRM.VALUE',
            ],
            'FieldsInIsRppMassiv' => [
                'MapPolOnamePolIsRpp' => 'IS_RPP.NAME',
                'MapPolIdPolIsRpp' => 'MAP_POL_ID_POL_IS_RPP.VALUE',
            ],
            'DataForLkMassiv' => [
                'MapNameTableLK' => 'MAP_NAME_TABLE_LK.VALUE',
                'MapPolOtobrazhatVLk' => 'MAP_POL_OTOBRAZHAT_V_LK.ITEM.VALUE',
            ],

        ];
    }

    public static function prepareFields($productItems, $userFields): array
    {
        $fields = self::setSelectedFields();

        $productItems['MapPolNamePolCrm'] = $userFields[$productItems['MapPolNamePolCrm']]
            ?? $productItems['MapPolNamePolCrm'];
        unset($fields['ID']);

        if (!empty($productItems['MapPolOtobrazhatVLk'])) {
            $productItems['MapPolOtobrazhatVLk'] = $productItems['MapPolOtobrazhatVLk'] === 'Да'
                ? 'true'
                : 'false';
        }

        return self::prepareFieldsRecursive($fields, $productItems);
    }
}
