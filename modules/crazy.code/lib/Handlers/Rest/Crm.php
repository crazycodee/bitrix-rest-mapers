<?php

namespace Crazy\Code\Handlers\Rest;

class Crm
{
    public static function onRestServiceBuildDescription(): array
    {
        return [
            'CrmRequest' => [
                'CrmRequest.GetProducts' => [CrmRequest\Products::class, 'getProducts'],
                'CrmRequest.GetFieldsMappingIsRpp' => [CrmRequest\FieldsMappingEntity::class, 'getFieldsInLk'],
            ],
        ];
    }
}
