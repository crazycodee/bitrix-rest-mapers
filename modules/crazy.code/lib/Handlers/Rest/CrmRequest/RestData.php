<?php

namespace Crazy\Code\Handlers\Rest\CrmRequest;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Engine\Response\Converter;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\Json;
use Bitrix\Rest\RestException;

trait RestData
{
    /**
     * @return array
     * @throws ArgumentException
     */
    public static function getJsonData(): array
    {
        return Json::decode(HttpRequest::getInput());
    }

    public static function convertToCamelCase(array $item): array
    {
        $converter = new Converter(Converter::TO_CAMEL);
        $arItem = [];
        foreach ($item as $key => $value) {
            if ($value instanceof DateTime) {
                $value = $value->format('Y-m-d\TH:i:s');
            }
            $arItem[$converter->process($key)] = $value;
        }
        return $arItem;
    }

    /**
     * @param array $data
     * @param array $reqFields
     *
     * @throws RestException
     */
    public static function checkRequiredFields(array $data, array $reqFields): void
    {
        $result = array_diff($reqFields, array_keys($data));
        if (!empty($result)) {
            self::throwError(Loc::getMessage('ERROR_REQUIRED_FIELDS', [
                '#FIELDS#' => implode(',', $result),
            ]));
        }
    }

    /**
     * @param string $message
     * @param string $code
     *
     * @return void
     * @throws RestException
     */
    public static function throwError(string $message, string $code = 'Error'): void
    {
        $message = str_replace(['"', '<br>'], ["'", ''], $message);
        $message = (string)preg_replace('/\s\\\\[\\\\\w]+/m', '', $message);
        global $APPLICATION;
        $APPLICATION->ResetException();
        throw new RestException($message, $code);
    }
}
