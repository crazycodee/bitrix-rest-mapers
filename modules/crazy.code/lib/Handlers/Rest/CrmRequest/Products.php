<?php
/** @bxnolanginspection */

namespace Crazy\Code\Handlers\Rest\CrmRequest;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Rest\RestException;
use Exception;
use Crazy\Code\Handlers\Rest\Maps\ProductMapFields;

class Products
{
    use RestData;

    /**
     * @return array
     * @throws RestException
     */
    public static function getProducts(): array
    {
        $result = [];
        try {
            $fieldsSubcategories = IBlocks::getQuery('Subcategories')
                ->whereNotNull('PRODUCT.IBLOCK_GENERIC_VALUE')
                ->setSelect(['PRODUCT.ELEMENT', 'NAME'])
                ->fetchCollection();

            $fieldsMappingIsRpp = IBlocks::getQuery('MappingIsRpp')
                ->setSelect(
                    [
                        'MAP_PRODUCT.ELEMENT',
                        'MAP_STATUS_GROUP.ITEM',
                        'MAP_PUB_IN_LK.ITEM',
                        'MAP_STATUS_CRM.VALUE',
                        'MAP_SERIAL_NUMBER.VALUE',
                        'MAP_CODE_STATUS_IS_RPP.ELEMENT',
                        'MAP_STATUS_IS_RPP.VALUE',
                    ]
                )
                ->addOrder('MAP_STATUS_GROUP.ITEM.SORT')
                ->fetchCollection();

            $iBlockProduct = IBlocks::getQuery('Products')
                ->setSelect(ProductMapFields::getSelectedFields())
                ->addSelect('DOCUMENTY_DLYA_PODACHI.FILE')
                ->addSelect('DOKUMENTY.FILE')
                ->addSelect('APPLICATION_TEMPLATE.FILE');

            $input = HttpRequest::getInput();
            if ($input) {
                $jsonData = self::getJsonData();
                $isRppUslugiIds = $jsonData['filter']['IdUslugiIsRpp'];
                if (!empty($isRppUslugiIds)) {
                    $fieldsProduct = $iBlockProduct
                        ->addSelect('ID_USLUGI_IS_RPP.VALUE')
                        ->whereIn('ID_USLUGI_IS_RPP.VALUE', $isRppUslugiIds);
                }
            }

            $fieldsProduct = $iBlockProduct->fetchCollection();

            $fieldsMappingIBlock = new IBlock('FieldsMapping');
            $yesValueId = $fieldsMappingIBlock->getEnumIdByXml('MAP_POL_OTOBRAZHAT_V_LK', 'Y');
            $fieldsMapping = IBlocks::getQuery('FieldsMapping')
                ->addSelect('MAP_POL_ID_POL_IS_RPP.VALUE', 'ID_FIELD_IS_RPP')
                ->addSelect('MAP_POL_PRODUCT.IBLOCK_GENERIC_VALUE', 'PRODUCT_ID')
                ->addSelect('MAP_NAME_TABLE_LK.VALUE', 'TABLE_NAME')
                ->addSelect('MAP_POL_OTOBRAZHAT_V_LK.VALUE')
                ->addSelect('MAP_POL_ONAME_POL_IS_RPP.VALUE', 'FIELD_ELEMENT_ID')
                ->registerRuntimeField(
                    new Reference(
                        'IS_RPP_FIELDS',
                        ElementTable::class,
                        Join::on('this.FIELD_ELEMENT_ID', 'ref.ID')
                    )
                )
                ->addSelect('IS_RPP_FIELDS.NAME', 'FIELD_NAME')
                ->addGroup('PRODUCT_ID')
                ->where('MAP_POL_OTOBRAZHAT_V_LK.VALUE', '=', $yesValueId)
                ->fetchAll();
        } catch (Exception $exception) {
            self::throwError($exception->getMessage());
        }

        foreach ($fieldsProduct as $key => $items) {
            $result['Product'][$key] = ProductMapFields::prepareFields(
                $items,
                $fieldsSubcategories,
                $fieldsMappingIsRpp,
                $fieldsMapping
            );
        }

        return $result;
    }
}
