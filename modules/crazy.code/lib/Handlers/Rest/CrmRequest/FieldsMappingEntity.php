<?php
/** @bxnolanginspection */

namespace Crazy\Code\Handlers\Rest\CrmRequest;

use Bitrix\Iblock\ElementPropertyTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\SystemException;
use Exception;
use Crazy\Code\Handlers\Rest\Maps\FieldsMappingEntityMapFields;


class FieldsMappingEntity
{
    use RestData;

    /**
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws ArgumentException
     */
    public static function getFieldsInLk()
    {
        $result = [];
        $product = new HelpIblock('Products');
        $subCatIblock = new HelpIblock('Subcategories');
        $isRppIblock = new HelpIblock('FieldsIsRpp');
        $subCatProp = $subCatIblock->getPropertyIdByCode('PRODUCT');
        $userFields = DealsFields::getValueList();
        $elementId = '';
        $elementKey = 0;
        $podcatName = [];

        try {
            $fieldsMappingEntityItems = IBlocks::getQuery('FieldsMapping')
                ->setSelect(FieldsMappingEntityMapFields::getSelectedFields())
                ->registerRuntimeField(
                    new Reference(
                        'IS_RPP',
                        $isRppIblock->getEntityDataClass(),
                        Join::on('this.MAP_POL_ONAME_POL_IS_RPP.IBLOCK_GENERIC_VALUE', 'ref.ID')
                    )
                )
                ->registerRuntimeField(
                    new Reference(
                        'PRODUCT',
                        $product->getEntityDataClass(),
                        Join::on('this.MAP_POL_PRODUCT.IBLOCK_GENERIC_VALUE', 'ref.ID')
                    )
                )
                ->registerRuntimeField(
                    new Reference(
                        'ELEMENT_PROPERTY',
                        ElementPropertyTable::class,
                        Join::on('this.PRODUCT.ID', 'ref.VALUE')
                            ->where('ref.IBLOCK_PROPERTY_ID', '=', $subCatProp)
                    )
                )
                ->registerRuntimeField(
                    new Reference(
                        'SUBCATEGORY',
                        $subCatIblock->getEntityDataClass(),
                        Join::on('this.ELEMENT_PROPERTY.IBLOCK_ELEMENT_ID', 'ref.ID')
                    )
                )
                ->fetchAll();
        } catch (Exception $exception) {
            self::throwError($exception->getMessage());
        }

        foreach ($fieldsMappingEntityItems as $items) {
            if ($items['ID'] === $elementId) {

                if (! in_array($items['PodcategoryId'], $podcatName)) {
                    $result['Product'][$elementKey - 1]['Podcategory'][] = [
                        'NamePodcategory' => $items['NamePodcategory'],
                        'PodcategoryId'   => $items['PodcategoryId'],
                    ];
                    $podcatName[] = $items['PodcategoryId'];
                }

                continue;
            }
            $result['Product'][] = FieldsMappingEntityMapFields::prepareFields($items, $userFields);

            $elementId = $items['ID'];
            $podcatName[] = $items['PodcategoryId'];
            $elementKey = count($result['Product']);
        }

        return $result;
    }
}
