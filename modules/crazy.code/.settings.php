<?php
return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Crazy\\Code\\Controller' => 'api',
            ],
        ],
        'readonly' => true,
    ],
];
